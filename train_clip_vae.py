"""Train a VAE plus Classifier on Event Clips dataset."""
import argparse
import tempfile
import logging
from tkinter import E
from typing import Dict, Union
import numpy as np
import torch
from torch.utils.data import DataLoader
import torchvision
# from line_profiler import LineProfiler

import os
from utils.VisdomPlotter import VisdomLinePlotter, plot_video_visdom, plot_jointvideo_visdom
import math
from datasets.SimpleClipDataset import SimpleClipDataset

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


BASE_PATH = os.path.dirname(__file__)


class LGMLoss(torch.nn.Module):
    def __init__(self, num_classes, feat_dim, alpha=0.1, lambda_=0.01):
        super(LGMLoss, self).__init__()
        self.num_classes = num_classes
        self.alpha = alpha
        self.lambda_ = lambda_
        self.means = torch.nn.Parameter(torch.randn(num_classes, feat_dim))
        torch.nn.init.xavier_uniform_(self.means, gain=math.sqrt(2.0))
        self.metric = AccuracyMetric()

    def forward(self, feat, labels=None):
        batch_size = feat.size()[0]

        XY = torch.matmul(feat, torch.transpose(self.means, 0, 1))
        XX = torch.sum(feat ** 2, dim=1, keepdim=True)
        YY = torch.sum(torch.transpose(self.means, 0, 1)**2, dim=0, keepdim=True)
        neg_sqr_dist = -0.5 * (XX - 2.0 * XY + YY)

        if labels is None:
            psudo_labels = torch.argmax(neg_sqr_dist, dim=1)
            means_batch = torch.index_select(self.means, dim=0, index=psudo_labels)
            likelihood_reg_loss = self.lambda_ * (torch.sum((feat - means_batch)**2) / 2) * (1. / batch_size)
            return likelihood_reg_loss, 0.0, means_batch

        labels_reshped = labels.view(labels.size()[0], -1)

        if torch.cuda.is_available():
            ALPHA = torch.zeros(batch_size, self.num_classes).cuda().scatter_(1, labels_reshped, self.alpha)
            K = ALPHA + torch.ones([batch_size, self.num_classes]).cuda()
        else:
            ALPHA = torch.zeros(batch_size, self.num_classes).scatter_(1, labels_reshped, self.alpha)
            K = ALPHA + torch.ones([batch_size, self.num_classes])

        logits_with_margin = torch.mul(neg_sqr_dist, K)
        means_batch = torch.index_select(self.means, dim=0, index=labels)
        likelihood_reg_loss = self.lambda_ * (torch.sum((feat - means_batch)**2) / 2) * (1. / batch_size)
        return likelihood_reg_loss, self.metric(logits_with_margin, labels), logits_with_margin



class MyAttentionS(torch.nn.Module):
    """Attention Module."""
    def __init__(self, insize, embedding_size, encoder_type="SAP"):
        super(MyAttentionS, self).__init__()
        self.insize = insize
        self.encoder_type = encoder_type
        self.embedding_size = embedding_size
        self.attention = torch.nn.Sequential(
            torch.nn.Conv1d(insize, embedding_size, kernel_size=1),
            torch.nn.ReLU(),  # MemoryEfficientSwish(),  # Swish(),  #
            torch.nn.BatchNorm1d(embedding_size),
            torch.nn.Conv1d(embedding_size, insize, kernel_size=1),
            torch.nn.Softmax(dim=2),
        )

    def getoutputsize(self):
        """Get attention style output dims."""
        out_dim = 0
        if self.encoder_type == "SAP":
            out_dim = self.insize
        elif self.encoder_type == "ASP":
            out_dim = self.insize * 2
        return out_dim

    def forward(self, x):
        """Run the attention on input data."""
        x = x.reshape(x.size()[0], -1, x.size()[-1])
        w = self.attention(x)
        if self.encoder_type == "SAP":
            x = torch.sum(x * w, dim=2)
        elif self.encoder_type == "ASP":
            mu = torch.sum(x * w, dim=2)
            sg = torch.sqrt((torch.sum((x**2) * w, dim=2) - mu**2).clamp(min=1e-5))
            x = torch.cat((mu, sg), 1)
        x = x.view(x.size()[0], -1)
        return x


class Reshape(torch.nn.Module):
    """Reshape in nn.Module format."""
    def __init__(self, outer_shape):
        super().__init__()
        self.outer_shape = outer_shape

    def forward(self, x):
        """Reshape the input to specified dims."""
        return x.view(x.size(0), *self.outer_shape)


class Flatten3d(torch.nn.Module):
    def forward(self, x):
        N, C, D, H, W = x.size()  # read in N, C, D, H, W
        return x.view(N, -1)  # "flatten" the C * D * H * W values into a single vector per image


class Cnn3D(torch.nn.Module):
    """Convolutional encoder for video."""
    def __init__(self, ishape, z_dim):
        super().__init__()
        self.model_base = torch.nn.Sequential([
            torch.nn.Conv3d(in_channels=ishape[1], out_channels=32, kernel_size=2, stride=1),  # [2, 32, 2, 149, 149]
            torch.nn.ReLU(inplace=True),  # [2, 32, 1, 74, 74]
            torch.nn.MaxPool3d((1, 2, 2), stride=2),
            torch.nn.Conv3d(in_channels=32, out_channels=64, kernel_size=(1, 3, 3), stride=1),  # [2, 64, 1, 72, 72]
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool3d((1, 3, 3), stride=2),  # [2, 64, 1, 35, 35]
            torch.nn.Conv3d(in_channels=64, out_channels=128, kernel_size=(1, 3, 3), stride=1),  # [2, 128, 1, 33, 33]
            torch.nn.ReLU(inplace=True),
            torch.nn.MaxPool3d((1, 3, 3), stride=2),  # [2, 128, 1, 16, 16]
            torch.nn.Dropout3d(0.1),
            torch.nn.Flatten3d(),
            torch.nn.ReLU(inplace=True),
            torch.nn.Linear(128 * 16 * 16, z_dim)
        ])

    def forward(self, x):
        """Run the encoder on data."""
        y = self.model_base(x)
        return y


class Encoder(torch.nn.Module):
    """Convolutional encoder."""
    def __init__(self, ishape, z_dim):
        super().__init__()
        # ishape [B,C,H,W]
        self.model = torch.nn.ModuleList([
            torch.nn.Conv2d(ishape[1], 64, 4, 2, padding=1),
            torch.nn.LeakyReLU(),
            torch.nn.Conv2d(64, 128, 4, 2, padding=1),
            torch.nn.LeakyReLU(),
            torch.nn.Conv2d(128, 128, 4, 2, padding=1),
            torch.nn.Flatten(),
            torch.nn.Linear(128 * int(ishape[2] / 8) * int(ishape[3] / 8), 1024),
            torch.nn.LeakyReLU(),
            torch.nn.Linear(1024, z_dim)
        ])

    def forward(self, x):
        """Run the encoder on data."""
        # print(x.shape)
        for layer in self.model:
            x = layer(x)
            # print(x.shape)
        return x


class EncoderMP(torch.nn.Module):
    """Convolutional encoder."""
    def __init__(self, ishape, z_dim):
        super().__init__()
        # ishape [B,C,H,W]
        self.model = torch.nn.ModuleList([
            torch.nn.Conv2d(ishape[1], 64, 3, 2, padding=1),
            torch.nn.LeakyReLU(),
            torch.nn.MaxPool2d((2, 2)),
            torch.nn.Conv2d(64, 128, 3, 1, padding=1),
            torch.nn.LeakyReLU(),
            torch.nn.MaxPool2d((2, 2)),
            torch.nn.Conv2d(128, 128, 3, 1, padding=1),
            torch.nn.LeakyReLU(),
            torch.nn.MaxPool2d((2, 2)),
            torch.nn.Flatten(),
            torch.nn.Linear(128 * (ishape[2] // 16) * (ishape[3] // 16), 1024),
            torch.nn.LeakyReLU(),
            torch.nn.Linear(1024, z_dim)
        ])

    def forward(self, x):
        """Run the encoder on data."""
        # print(x.shape)
        for layer in self.model:
            x = layer(x)
            # print(x.shape)
        return x


class EncoderNet(torch.nn.Module):
    """Convolutional encoder."""
    def __init__(self, ishape, z_dim, hidden_dims=None):
        super().__init__()
        # ishape [B,C,H,W]
        in_channels = ishape[1]

        modules = []
        if hidden_dims is None:
            hidden_dims = [32, 64, 128, 256, 512]
        for h_dim in hidden_dims:
            modules.append(
                torch.nn.Sequential(
                    torch.nn.Conv2d(in_channels, out_channels=h_dim,
                                    kernel_size=3, stride=2, padding=1),
                    torch.nn.BatchNorm2d(h_dim),
                    torch.nn.LeakyReLU())
            )
            in_channels = h_dim
        self.encoder = torch.nn.Sequential(*modules)

    def forward(self, x):
        """Run the encoder on data."""
        result = self.encoder(x)
        return result


class DecoderNet(torch.nn.Module):
    """Convolutional encoder."""
    def __init__(self, ishape, z_dim, hidden_dims=None):
        super().__init__()
        # ishape [B,C,H,W]
        modules = []
        self.decoder_input = torch.nn.Linear(
            z_dim, (128 // 30) * (128 // 30) * 512)
        if hidden_dims is None:
            hidden_dims = [512, 256, 128, 64, 32]
        for i in range(len(hidden_dims) - 1):
            modules.append(
                torch.nn.Sequential(
                    torch.nn.ConvTranspose2d(hidden_dims[i],
                                             hidden_dims[i + 1],
                                             kernel_size=3,
                                             stride=2,
                                             padding=1,
                                             output_padding=1),
                    torch.nn.BatchNorm2d(hidden_dims[i + 1]),
                    torch.nn.LeakyReLU())
            )
        self.decoder = torch.nn.Sequential(*modules)
        self.final_layer = torch.nn.Sequential(
            torch.nn.ConvTranspose2d(hidden_dims[-1],
                                     hidden_dims[-1],
                                     kernel_size=3,
                                     stride=2,
                                     padding=1,
                                     output_padding=1),
            torch.nn.BatchNorm2d(hidden_dims[-1]),
            torch.nn.LeakyReLU(),
            torch.nn.Conv2d(hidden_dims[-1], out_channels=3,
                            kernel_size=3, padding=1),
            torch.nn.Tanh())

    def forward(self, x):
        """Run the encoder on data."""
        d = self.decoder_input(x)
        d = d.view(-1, 512, (128 // 30), (128 // 30))
        result = self.decoder(d)
        result = self.final_layer(result)
        return result


class Decoder(torch.nn.Module):
    """Convolutional Decoder, reconstruct the image."""
    def __init__(self, ishape, z_dim):
        super().__init__()
        self.model = torch.nn.ModuleList([
            torch.nn.Linear(z_dim, 1024),
            torch.nn.ReLU(),
            torch.nn.Linear(1024, 128 * int(ishape[2] / 8) * int(ishape[3] / 8)),
            torch.nn.ReLU(),
            Reshape((128, int(ishape[2] / 8), int(ishape[3] / 8),)),
            torch.nn.ConvTranspose2d(128, 128, 4, 2, padding=0),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(128, 64, 4, 2, padding=1),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose2d(64, ishape[1], 4, 2, padding=0),
            torch.nn.Sigmoid()
        ])

    def forward(self, x):
        """Run the Decoder on data."""
        # print(x.shape)
        for layer in self.model:
            x = layer(x)
            print(x.shape)
        return x


class IRMLinear(torch.nn.Module):
    """Implicid Rank Minimizer linear layers."""
    def __init__(self, fin, n_layers=4):
        super().__init__()
        self.fin = fin
        self.block = torch.nn.Sequential(
            *[torch.nn.Linear(fin, fin, bias=False) for _ in range(n_layers)])
        # for l in self.block:
        #     nn.init.normal_(l.weight, 0, 0.5 / self.fin ** 0.5)
        self.compressed_block = None

    def get_output_size(self):
        """Get output dim."""
        return self.fin

    def forward(self, x):
        """Run the Implicid Rank Minimizer linear layers on data."""
        if self.training:
            self.compressed_block = None
            return self.block(x)
        else:
            if self.compressed_block is None:
                with torch.no_grad():
                    compressed_block = self.block[0].weight.t()
                    for clayer in self.block[1:]:
                        compressed_block = compressed_block @ clayer.weight.t()
                    self.compressed_block = compressed_block.t()
            return torch.nn.functional.linear(x, self.compressed_block)


class Classifier(torch.nn.Module):
    """Fully connected layers."""
    def __init__(self, z_dim, compression=2):
        super().__init__()
        self.internal_size = z_dim // compression
        self.model = torch.nn.ModuleList([
            torch.nn.Linear(z_dim, self.internal_size),
            torch.nn.ReLU(),
            torch.nn.BatchNorm1d(self.internal_size),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(self.internal_size, self.internal_size)
        ])

    def get_output_size(self):
        """Get output tensor dim."""
        return self.internal_size

    def forward(self, x):
        """Run Classifier on data."""
        # print(x.shape)
        for layer in self.model:
            x = layer(x)
        #    print(x.shape)
        return x


class ClassifierNELU(torch.nn.Module):
    """Fully connected Layers running with ELU instead of ReLU."""
    def __init__(self, z_dim, compression=2):
        super().__init__()
        self.internal_size = z_dim // compression
        self.model = torch.nn.ModuleList([
            torch.nn.Linear(z_dim, self.internal_size),
            torch.nn.ELU(),
            # torch.nn.BatchNorm1d(z_dim),
            # torch.nn.Dropout(0.2),
            # torch.nn.Linear(self.internal_size, self.internal_size)
        ])

    def get_output_size(self):
        """Get tensor output size."""
        return self.internal_size

    def forward(self, x):
        """Run the layers on data."""
        # print(x.shape)
        for layer in self.model:
            x = layer(x)
        #    print(x.shape)
        return x


def compute_kernel(x, y):
    """Kernel trick for Maximum Mean Discrepancy calculation."""
    x_size = x.size(0)
    y_size = y.size(0)
    dim = x.size(1)
    x = x.unsqueeze(1)  # (x_size, 1, dim)
    y = y.unsqueeze(0)  # (1, y_size, dim)
    tiled_x = x.expand(x_size, y_size, dim)
    tiled_y = y.expand(x_size, y_size, dim)
    kernel_input = (tiled_x - tiled_y).pow(2).mean(2) / float(dim)
    return torch.exp(-kernel_input)  # (x_size, y_size)


def compute_mmd(x, y):
    """Maximum Mean Discrepancy loss."""
    x_kernel = compute_kernel(x, x)
    y_kernel = compute_kernel(y, y)
    xy_kernel = compute_kernel(x, y)
    mmd = x_kernel.mean() + y_kernel.mean() - 2 * xy_kernel.mean()
    return mmd


class VaeModel(torch.nn.Module):
    """Variational AutoEncoder using MMD."""
    def __init__(self, input_dim, z_dim):
        super().__init__()
        self.internal_dim = z_dim
        self.encoder = Encoder(input_dim, z_dim)
        self.decoder = Decoder(input_dim, z_dim)

    def variational_loss(self, bdim, z, use_cuda=1):
        """Variational loss for the autoencoder."""
        true_samples = torch.randn(bdim, self.internal_dim, requires_grad=False)
        if use_cuda == 1:
            true_samples = true_samples.cuda()
        mmd = compute_mmd(true_samples, z)
        return mmd

    def forward(self, x):
        """Run the encoder on data and return latent and reconstruction."""
        z = self.encoder(x)
        x_reconstructed = self.decoder(z)
        return z, x_reconstructed


class VaeModelExtra(torch.nn.Module):
    """Variational AutoEncoder using MMD."""
    def __init__(self, input_dim, z_dim):
        super().__init__()
        self.internal_dim = z_dim
        self.encoder = EncoderNet(input_dim, z_dim)
        self.fc_z = torch.nn.Linear(
            (128 // 30) * (128 // 30) * 512,
            self.internal_dim)
        self.decoder = DecoderNet(input_dim, z_dim)

    def variational_loss(self, bdim, z, use_cuda=1):
        """Variational loss for the autoencoder."""
        true_samples = torch.randn(bdim, self.internal_dim, requires_grad=False)
        if use_cuda == 1:
            true_samples = true_samples.cuda()
        mmd = compute_mmd(true_samples, z)
        return mmd

    def encode(self, x):
        e = self.encoder(x)
        e = torch.flatten(e, start_dim=1)
        z = self.fc_z(e)
        return z

    def forward(self, x):
        """Run the encoder on data and return latent and reconstruction."""
        e = self.encoder(x)
        e = torch.flatten(e, start_dim=1)
        z = self.fc_z(e)
        x_reconstructed = self.decoder(z)
        return z, x_reconstructed


class VaeModelMP(torch.nn.Module):
    """Variational AutoEncoder using MMD."""
    def __init__(self, input_dim, z_dim):
        super().__init__()
        self.internal_dim = z_dim
        self.encoder = EncoderMP(input_dim, z_dim)
        self.decoder = Decoder(input_dim, z_dim)

    def variational_loss(self, bdim, z, use_cuda=1):
        """Variational loss for the autoencoder."""
        true_samples = torch.randn(bdim, self.internal_dim, requires_grad=False)
        if use_cuda == 1:
            true_samples = true_samples.cuda()
        mmd = compute_mmd(true_samples, z)
        return mmd

    def forward(self, x):
        """Run the encoder on data and return latent and reconstruction."""
        z = self.encoder(x)
        x_reconstructed = self.decoder(z)
        return z, x_reconstructed


class VanillaVaeModel(torch.nn.Module):
    """Variational AutoEncoder using KLD."""
    def __init__(self, input_dim, z_dim):
        super().__init__()
        self.internal_dim = z_dim
        self.encoder = Encoder(input_dim, z_dim)
        self.mu = torch.nn.Linear(z_dim, z_dim)
        self.sigma = torch.nn.Linear(z_dim, z_dim)
        self.decoder = Decoder(input_dim, z_dim)

    def reparameterize(self, mu: torch.Tensor, logvar: torch.Tensor) -> torch.Tensor:
        """Reparameterization trick to sample from N(mu, var) from N(0,1).

        Args:
            param mu: (Tensor) Mean of the latent Gaussian [B x D]
            param logvar: (Tensor) Standard deviation of the latent Gaussian [B x D]
        Returns:
            (Tensor) [B x D]
        """
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu

    def variational_loss(self, bdim, z, use_cuda=1):
        """KLD Variational loss."""
        mu, logvar = torch.split(z, self.internal_dim, dim=1)
        kld_loss = torch.mean(-0.5 * torch.sum(1 + logvar - mu ** 2 - logvar.exp(), dim=1), dim=0)
        return kld_loss

    def forward(self, x):
        """Run the VAE on data."""
        l: torch.Tensor = self.encoder(x)
        mu = self.mu(l)
        logvar = self.sigma(l)
        z = self.reparameterize(mu, logvar)
        x_reconstructed = self.decoder(z)
        return torch.cat([mu, logvar], dim=1), x_reconstructed


class IRMVaeModel(VaeModel):
    """Variational AutoEncoder using MMD with IRM Layers."""
    def __init__(self, input_dim, z_dim):
        super().__init__()
        self.internal_dim = z_dim
        self.encoder = Encoder(input_dim, z_dim)
        # https://proceedings.neurips.cc/paper/2020/file/a9078e8653368c9c291ae2f8b74012e7-Paper.pdf
        self.IRM = IRMLinear(z_dim)
        self.decoder = Decoder(input_dim, z_dim)

    def forward(self, x):
        """Run the model with data."""
        z = self.encoder(x)
        irmz = self.IRM(z)
        x_reconstructed = self.decoder(irmz)
        return irmz, x_reconstructed


class SparseAEModel(VaeModelExtra):
    """Variational AutoEncoder using MMD."""
    def __init__(self, input_dim, z_dim):
        super().__init__(input_dim, z_dim)

    def variational_loss(self, bdim, z, use_cuda=1):
        """Sparsity loss on AE."""
        rho = 0.05
        dkl = - rho * torch.log(self.data_rho) - (1-rho)*torch.log(1-self.data_rho) # calculates KL divergence
        rho_loss = dkl.mean()
        return rho_loss

    def encode(self, x):
        z, x_reconstructed = super().forward(x)
        z = torch.sigmoid(z)
        return z

    def forward(self, x):
        """Run the encoder on data and return latent and reconstruction."""
        z, x_reconstructed = super().forward(x)
        z = torch.sigmoid(z)
        self.data_rho = z.mean(0)
        return z, x_reconstructed


class AccuracyMetric:
    """Works with classification model."""

    def __init__(self):
        self.last_value: int = 0
        self.avalue: float = 0.0
        self.total: int = 0

    def __call__(self, outputs, target):
        """Compute accuracy."""
        if type(target) in (tuple, list):
            target = target[0]
        _, predicted = torch.max(outputs.data, 1)
        correct = (predicted == target).sum().item()
        total = target.numel()
        pacc = 100 * float(correct) / total
        self.last_value = pacc
        return pacc

    def add(self, acc_percentage, num_samples):
        prob = acc_percentage / 100
        total_samples = self.total + num_samples
        self.avalue = (self.avalue * self.total +
                       prob * num_samples) / total_samples
        self.total = total_samples
        return self.avalue

    def reset(self):
        """Reset counters."""
        self.last_value = 0
        self.avalue = 0.0
        self.total = 0
        return

    def value(self):
        """Return accuracy percentage."""
        return self.avalue * 100

    def get_last_value(self):
        """Return accuracy value."""
        return self.last_value

    def name(self):
        """Return metric name."""
        return 'Accuracy'

    def requieres(self):
        """Return requierements."""
        return 'logits'


def l2_norm(input, axis=1):
    """Compute L2 norm for the input Tensor."""
    norm = torch.norm(input, 2, axis, True)
    output = torch.div(input, norm)
    return output


class Amsoftmax(torch.nn.Module):
    """implementation of additive margin softmax loss.

    https://arxiv.org/abs/1801.05599
    """
    def __init__(self, embedding_size=512,
                 classnum=51332,
                 device='cuda',
                 s=30., m=0.5,
                 gamma=0, name='Am_softmax',
                 **kwargs):
        super().__init__()
        self.classnum = classnum
        self.kernel = torch.nn.Parameter(torch.FloatTensor(embedding_size, classnum).to(device))
        # initial kernel
        torch.nn.init.xavier_uniform_(self.kernel)

        self.m = m  # additive margin recommended by the paper
        self.s = s  # see normface https://arxiv.org/abs/1704.06369

        if 'classWeights' in kwargs:
            self.loss = torch.nn.CrossEntropyLoss(weight=kwargs['classWeights'])
        else:
            self.loss = torch.nn.CrossEntropyLoss()
        self.name = name

        self.metric = AccuracyMetric()
        self.iteration = 0

    def forward(self, embbedings, label):
        """Run loss on embeddings."""
        kernel_norm = l2_norm(self.kernel, axis=0)
        cos_theta = torch.mm(embbedings, kernel_norm)
        cos_theta = cos_theta.clamp(-1, 1)  # for numerical stability

        phi = cos_theta - self.m

        one_hot = torch.zeros_like(cos_theta)
        one_hot.scatter_(1, label.view(-1, 1).long(), 1)
        output = cos_theta * (1 - one_hot) + phi * one_hot
        output *= self.s  # scale up in order to phi softmax work, first introduced in normface
        return self.loss(output, label), self.metric(cos_theta, label), output


class Aamsoftmax(torch.nn.Module):
    """implementation of additive margin softmax loss.

    https://arxiv.org/abs/1801.05599
    """
    def __init__(self, embedding_size=512,
                 classnum=51332, device='cuda', s=30., m=0.5, gamma=0, **kwargs):
        super().__init__()
        self.classnum = classnum
        self.kernel = torch.nn.Parameter(torch.FloatTensor(classnum, embedding_size).to(device),
                                         requires_grad=True)
        self.gamma = gamma

        # initial kernel
        torch.nn.init.xavier_normal_(self.kernel, gain=1)
        self.m = m  # additive margin recommended by the paper
        self.s = s  # see normface https://arxiv.org/abs/1704.06369

        self.cos_m = math.cos(self.m)
        self.sin_m = math.sin(self.m)

        self.th = math.cos(math.pi - self.m)
        self.mm = math.sin(math.pi - self.m) * self.m

        if 'classWeights' in kwargs:
            self.ce = torch.nn.CrossEntropyLoss(weight=kwargs['classWeights'])
        else:
            self.ce = torch.nn.CrossEntropyLoss()
        self.metric = AccuracyMetric()

    def forward(self, embbedings, label):
        """Run loss on embeddings."""
        cosine = torch.nn.functional.linear(
            torch.nn.functional.normalize(embbedings),
            torch.nn.functional.normalize(self.kernel))
        sine = torch.sqrt((1.0 - torch.pow(cosine, 2)).clamp(0, 1))
        # cos(a+b) = cos(a)cos(b) - sin(a)sin(b)
        phi = cosine * self.cos_m - sine * self.sin_m
        phi = torch.where((cosine - self.th) > 0, phi, cosine - self.mm)

        one_hot = torch.zeros_like(cosine)
        one_hot.scatter_(1, label.view(-1, 1), 1)

        output = (one_hot * phi) + ((1.0 - one_hot) * cosine)
        output = output * self.s

        loss = self.ce(output, label)

        return loss, self.metric(cosine, label), output


class SMloss(torch.nn.Module):
    """Softmax loss with angular optional."""
    def __init__(
            self, in_features, out_features,
            device='cuda', s=10.0, m=0.0, gamma=0, angular=False, eps=1e-10,
            **kwargs):
        super(SMloss, self).__init__()

        self.fc = torch.nn.Parameter(
            torch.FloatTensor(in_features, out_features).to(device), requires_grad=True)
        torch.nn.init.xavier_uniform_(self.fc)
        self.in_features = in_features
        self.out_features = out_features
        self.eps = eps
        self.loss = torch.nn.CrossEntropyLoss()
        self.angular = angular
        self.basescaling = s
        self.s = s
        self.metric = AccuracyMetric()

    def forward(self, x, label):
        """Run loss on data."""
        if self.angular:
            fc = l2_norm(self.fc, axis=0)
            output = x.matmul(fc)
            output = output.clamp(-1, 1)  # for numerical stability
        else:
            output = x.matmul(self.fc)
        return self.loss(self.s * output, label), self.metric(output, label), output


def save_model(epoch, model, transform, discriminator, classifier, filename):
    """Save the checkpoint."""
    save_dict = {
        'epoch': epoch,
        'model_state_dict': model.state_dict(),
        'discriminator': discriminator.state_dict(),
        'transform': transform.state_dict(),
        'classifier': classifier.state_dict()}
    torch.save(save_dict, filename)


def write_to_file(f, data_to_write):
    """Append/Write a batch of vectors to file in tsv format."""
    for embedding in data_to_write:
        f.write('\t'.join([str(value) for value in embedding]) + '\n')


def optimizer_to_cuda(optimizer):
    """Place the optimizer params into cuda device."""
    for state in optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.cuda()


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Test EventVideoSequenceDataset \
                                                  on a video producing mini clips")
    parser.add_argument(
        '--list_file', type=str,
        default="/training-data/ciro_gracia/code/" +
                "optima-workspace/vision/data/datasets/mp4_list.lst")
    parser.add_argument(
        '--test_list_file', type=str,
        default="/training-data/ciro_gracia/code/" +
                "optima-workspace/vision/data/datasets/mp4_list.lst")

    # operation
    parser.add_argument('--onlyproject', type=int, default=0, help='project only')

    # model
    parser.add_argument('--checkpoint', type=str, default=None, help='checkpoint filename')
    parser.add_argument('--vae_latent_dims', type=int, default=256, help='dims in vae')
    parser.add_argument('--att_latent_dims', type=int, default=64, help='dims in att')
    parser.add_argument('--vae_loss_weight', type=float, default=1, help='variational loss weight')

    # train params
    parser.add_argument('--loss_name', type=str, default='sm', help='[sm,am,aam]')
    parser.add_argument('--use_cuda', type=int, default=1, help='use cuda?')
    parser.add_argument('--batch_size', type=int, default=64, help='clips batch size')
    parser.add_argument('--learning_rate', type=float, default=1e-3, help='train lr')
    parser.add_argument('--weight_decay', type=float, default=1e-5, help='train wd')
    parser.add_argument('--num_epoch', type=int, default=300, help='epoch length of the train')
    parser.add_argument('--dl_workers', type=int,
                        default=10, help='number of workers for dataloader')
    parser.add_argument('--cuda_device', type=int, default=0, help='[0 to N-1 ] gpu device to run')

    # print
    parser.add_argument('--print_every', type=int, default=1, help='print every epoch #')

    # validation
    parser.add_argument('--validate_every', type=int, default=2, help='validate every epoch #')

    # visualization
    parser.add_argument('--environment_name',
                        type=str, default="test_vae_", help='environment for visdom')
    parser.add_argument('--visdom_port', type=int, default=6690, help='port for visdom server')
    parser.add_argument('--use_visdom', type=int, default=1, help='visualize using visdom')
    parser.add_argument('--plot_every',
                        type=int, default=20, help='interval for visualization in epoch')

    # saving
    parser.add_argument('--model_filename',
                        type=str,
                        default="/training-data/ciro_gracia/code/" +
                                "optima-workspace/vision/data/datasets/vae",
                        help='output path for model')
    parser.add_argument('--embedings_filename',
                        type=str,
                        default="/training-data/ciro_gracia/code/" +
                                "optima-workspace/vision/data/datasets/vae.tsv",
                        help='output path for embeddings')

    return parser.parse_args()

# TODO: add validation to avoid overfitting
# TODO: checkpoints per epoch
# TODO: classification error % Accuracy, Confusion Matrix
# TODO: detection error : R.O.C
# TODO: average recostruction error for whole dataset
# TODO: augmentacio dels videos


def normalize_data(action_image_batch):

    mus = action_image_batch.mean([2, 3]).unsqueeze(2).unsqueeze(3)
    sigmas = action_image_batch.std([2, 3]).unsqueeze(2).unsqueeze(3)
    return mus, sigmas, (action_image_batch - mus) / (sigmas + 1e-7)


def validation_step(numclasses, testdl, model, transform, discriminator, classification, epoch):
    """Run the validation epoch."""
    num_samples = 0
    num_images = 0
    total_closs = 0
    total_l2loss = 0
    total_varloss = 0
    total_cmetric = 0

    # requiered for dropouts and batch norms
    model.eval()
    transform.eval()
    discriminator.eval()
    classification.eval()

    confusion = torch.zeros((numclasses, numclasses)).cuda()
    with torch.no_grad():
        # normalizer = torch.nn.InstanceNorm2d(3).cuda()
        for item, (action_frames_batch, current_event_type) in enumerate(testdl):
            # clip batch
            action_frames_batch = action_frames_batch.permute(0, 1, 4, 2, 3)
            slist = list(action_frames_batch.shape)
            bdims = [slist[1] * slist[0], slist[2], slist[3], slist[4]]

            # image batch
            action_image_batch = action_frames_batch.view(bdims).to(dtype=torch.float32)
            action_image_batch = (action_image_batch / 255)
            if use_cuda == 1:
                action_image_batch = action_image_batch.cuda()
                current_event_type = current_event_type.cuda()

            # action_image_batch = normalizer(action_image_batch)
            _, _, action_image_batch = normalize_data(action_image_batch)
            num_images += action_image_batch.shape[0]
            num_samples += action_frames_batch.shape[0]

            # VAE optimization
            z, x_reconstructed = model(action_image_batch)
            varloss = model.variational_loss(bdims[0], z, use_cuda)

            l2 = torch.nn.functional.mse_loss(x_reconstructed, action_image_batch)
            l1 = torch.nn.functional.l1_loss(x_reconstructed, action_image_batch)

            total_l2loss += (l2 + l1) * action_image_batch.shape[0]
            total_varloss += varloss * action_image_batch.shape[0]

            # loss = nll
            # Clip classification optimization
            vz = z.view(slist[0], -1, zdim).permute(0, 2, 1).detach()
            dvz = discriminator(transform(vz))
            closs, cmetric, logits = classification(dvz, current_event_type)

            logits = torch.nn.functional.softmax(logits, dim=1)
            one_hot = torch.zeros_like(logits)
            one_hot.scatter_(1, current_event_type.view(-1, 1).long(), 1)
            acc = torch.mm(logits.T, one_hot)
            confusion += acc / (1e-7 + torch.sum(acc, dim=0))  # colum are labels
            print(f"{confusion}")
            total_closs += closs * action_frames_batch.shape[0]
            total_cmetric += cmetric * action_frames_batch.shape[0]

    # reset and stats
    model.train()
    transform.train()
    discriminator.train()
    classification.train()
    average_cmetric = total_cmetric / num_samples
    average_varloss = total_varloss.cpu() / num_images
    average_l2loss = total_l2loss.cpu() / num_images
    average_closs = total_closs.cpu() / num_samples
    print(f"Eval epoch : {average_closs}, {average_cmetric}  {average_l2loss}, {average_varloss}")
    plotter.plot("L2loss", 'test', 'L2loss', epoch, average_l2loss)
    plotter.plot("MMD", 'test', 'MMD', epoch, average_varloss)
    plotter.plot("batch-accuracy", 'test', 'batch-accuracy', epoch, average_cmetric)
    return average_l2loss, average_varloss, average_closs, average_cmetric


def project_data(dataloader, model, transform, discriminator, classification, filename):
    """Compute and save embeddings into files."""
    model.eval()
    transform.eval()
    discriminator.eval()
    classification.eval()

    total_ve = []
    total_dve = []
    total_labels = []

    with torch.no_grad():
        for item, (action_frames_batch, current_event_type) in enumerate(dataloader):
            # clip batch
            action_frames_batch = action_frames_batch.permute(0, 1, 4, 2, 3)
            slist = list(action_frames_batch.shape)
            bdims = [slist[1] * slist[0], slist[2], slist[3], slist[4]]

            # image batch
            action_image_batch = action_frames_batch.view(bdims).to(dtype=torch.float32)
            action_image_batch = (action_image_batch / 255)
            if use_cuda == 1:
                action_image_batch = action_image_batch.cuda()
                current_event_type = current_event_type.cuda()

            _, _, action_image_batch = normalize_data(action_image_batch)
            z, _ = model(action_image_batch)
            vz = z.view(slist[0], -1, z.shape[1]).permute(0, 2, 1).detach()
            ve = transform(vz)

            dve = discriminator(ve)
            total_ve.append(ve)
            total_dve.append(dve)
            total_labels.append(current_event_type)

    with open(filename + "_lab.tsv", 'w') as f:
        tl = torch.cat(total_labels, dim=0).cpu().numpy()
        print(tl.shape)
        write_to_file(f, tl[np.newaxis, :])
    with open(filename + "_ve.tsv", 'w') as f:
        write_to_file(f, torch.cat(total_ve).cpu().numpy())
    with open(filename + "_dve.tsv", 'w') as f:
        write_to_file(f, torch.cat(total_dve).cpu().numpy())

    model.train()
    transform.train()
    discriminator.train()
    classification.train()


if __name__ == "__main__":

    params = parse_args()
    print_every = params.print_every
    plot_every = params.plot_every
    batch_size = params.batch_size
    use_cuda = params.use_cuda
    learning_rate = params.learning_rate
    weight_decay = params.weight_decay
    environment_name = params.environment_name
    zdim = params.vae_latent_dims
    num_epoch = params.num_epoch
    dl_numworkers = params.dl_workers
    att_ld = params.att_latent_dims
    validate_every = params.validate_every
    vae_loss_weight = params.vae_loss_weight

    plotter = VisdomLinePlotter(params.visdom_port, env_name=environment_name)

    # data pipeline
    dataset: SimpleClipDataset = SimpleClipDataset(params.list_file)
    numclasses: int = int(dataset.numclasses())
    print(f"len of the dataset is {len(dataset)}")
    test_dataset: SimpleClipDataset = SimpleClipDataset(params.test_list_file)
    test_numclasses: int = int(test_dataset.numclasses())
    print(f"len of the dataset is {len(test_dataset)}")

    # [L,W,H,C]
    dl = DataLoader(dataset, batch_size=batch_size,
                    num_workers=dl_numworkers, shuffle=True, drop_last=True)
    testdl = DataLoader(test_dataset,
                        batch_size=batch_size,
                        num_workers=dl_numworkers, shuffle=False)

    sample, sample_label = dataset[0]
    bdims = list(sample.shape)
    bdims = [bdims[0] * batch_size, bdims[-1], bdims[1], bdims[2]]

    # images vae, clip sumarizer and classfication loss
    # model = VaeModel(bdims, zdim)
    # model = VaeModelMP(bdims, zdim)
    model = VaeModelExtra(bdims, zdim)
    # model = VanillaVaeModel(bdims, zdim)
    # model = SparseAEModel(bdims, zdim)
    transform = MyAttentionS(zdim, att_ld, encoder_type='SAP')
    discriminator = ClassifierNELU(transform.getoutputsize(), compression=1)

    classification: Union[SMloss, Amsoftmax, Aamsoftmax]
    if params.loss_name == 'sm':
        classification = SMloss(in_features=discriminator.get_output_size(),
                                out_features=numclasses)
    elif params.loss_name == 'am':
        classification = Amsoftmax(embedding_size=discriminator.get_output_size(),
                                   classnum=numclasses, m=0.2)
    elif params.loss_name == 'lgm':
        classification = LGMLoss(numclasses, zdim, alpha=0)
    else:
        classification = Aamsoftmax(embedding_size=discriminator.get_output_size(),
                                    classnum=numclasses, m=0.2)


    #frameclassification = Amsoftmax(embedding_size=zdim,
    #                                classnum=numclasses, m=0.2)
    frameclassification = LGMLoss(zdim, zdim, alpha=0)

    if params.checkpoint is not None:
        print(f"loading checkpoint {params.checkpoint}")
        checkpoint = torch.load(params.checkpoint, map_location=torch.device('cpu'))
        if 'model_state_dict' in checkpoint:
            print("found vae")
            model.load_state_dict(checkpoint['model_state_dict'])
        if 'transform' in checkpoint:
            print("found transform")
            transform.load_state_dict(checkpoint['transform'])
        if 'discriminator' in checkpoint:
            print("found discriminator")
            discriminator.load_state_dict(checkpoint['discriminator'])
        if 'classifier' in checkpoint:
            print("found classifier")
            classification.load_state_dict(checkpoint['classifier'])

    if use_cuda == 1:
        model = model.cuda()
        classification = classification.cuda()
        discriminator = discriminator.cuda()
        transform = transform.cuda()
        frameclassification = frameclassification.cuda()

    if params.onlyproject == 1:
        project_data(dl, model, transform,
                     discriminator, classification, params.embedings_filename)
        exit()

    # TODO: check different weight decay values
    # TODO: check for diferent optimizers : SGD, AdaGrad
    modelparams = [p for p in model.parameters() if p.requires_grad]
    # modelparams = [p for p in model.decoder.parameters() if p.requires_grad]
    optimizer = torch.optim.Adam(modelparams, lr=learning_rate,
                                 weight_decay=weight_decay, eps=1e-6, amsgrad=True)

    cmodelparams = [p for p in classification.parameters() if p.requires_grad]
    transformparams = [p for p in transform.parameters() if p.requires_grad]
    discriminatorparams = [p for p in discriminator.parameters() if p.requires_grad]
    allparams = [{'params': cmodelparams, 'lr': learning_rate, 'weight_decay': 0},
                 {'params': transformparams, 'lr': learning_rate, 'weight_decay': 0},
                 {'params': discriminatorparams, 'lr': learning_rate, 'weight_decay': 0}]
                 # {'params': modelparams, 'lr': learning_rate, 'weight_decay': weight_decay}]
    coptimizer = torch.optim.Adam(allparams, lr=learning_rate,
                                  eps=1e-6, amsgrad=True)
    # coptimizer = torch.optim.SGD(allparams, lr=learning_rate)

    if use_cuda == 1:
        optimizer_to_cuda(optimizer)
        optimizer_to_cuda(coptimizer)

    # IMAGE NORMALIZATION
    #normalizer = torch.nn.BatchNorm2d(3).cuda()
    #normalizer = torch.nn.InstanceNorm2d(3).cuda()

    best_cmetric: int = 0
    for epoch in range(0, num_epoch):
        for item, (action_frames_batch, current_event_type) in enumerate(dl):

            # clip batch
            action_frames_batch = action_frames_batch.permute(0, 1, 4, 2, 3)
            slist = list(action_frames_batch.shape)
            bdims = [slist[1] * slist[0], slist[2], slist[3], slist[4]]

            # image batch
            action_image_batch = action_frames_batch.view(bdims)
            action_image_batch = (action_image_batch / 255)
            frame_event_type = current_event_type.repeat_interleave(7)
            if use_cuda == 1:
                action_image_batch = action_image_batch.cuda()
                current_event_type = current_event_type.cuda()
                frame_event_type = frame_event_type.cuda()

            # VAE optimization
            optimizer.zero_grad()
            coptimizer.zero_grad()

            mus, sigmas, action_image_batch = normalize_data(action_image_batch)
            z, x_reconstructed = model(action_image_batch)
            variational_loss = model.variational_loss(bdims[0], z, use_cuda)
            # l2loss = (x_reconstructed - action_image_batch).pow(2).mean()
            # an alternative loss
            l2 = torch.nn.functional.mse_loss(x_reconstructed, action_image_batch)
            l1 = torch.nn.functional.l1_loss(x_reconstructed, action_image_batch)
            t = x_reconstructed - action_image_batch
            l2loss = 1 * t + \
                torch.log(1. + torch.exp(- 2 * 1 * t)) - \
                torch.log(torch.tensor(2.0))
            l2loss = (1. / 1) * l2loss.mean()
            fcloss, fcmetric, _ = frameclassification(z, None)
            # loss = l2loss + l2 + l1 + variational_loss * vae_loss_weight
            # loss = l2 + l1 + variational_loss * vae_loss_weight
            loss = l2loss + l1 + variational_loss * vae_loss_weight + fcloss

            # loss = l1 + variational_loss * vae_loss_weight
            # loss = l2 + variational_loss * vae_loss_weight
            # loss = l2loss + variational_loss * vae_loss_weight
            loss.backward()
            optimizer.step()

            # Clip classification optimization
            vz = z.view(slist[0], -1, zdim).permute(0, 2, 1).detach()
            # z = model.encode(action_image_batch)
            # vz = z.view(slist[0], -1, z.shape[1]).permute(0, 2, 1)
            dvz = discriminator(transform(vz))
            closs, cmetric, _ = classification(dvz, current_event_type)
            closs.backward()
            coptimizer.step()

            if epoch % print_every == 0 and item == 0:
                plotter.plot("L2loss", 'train', 'L2loss', epoch, l2loss.item())
                plotter.plot("MMD", 'train', 'MMD', epoch, variational_loss.item())
                plotter.plot("batch-accuracy", 'train', 'batch-accuracy', epoch, cmetric)
                print("{}/{} ({}/{}) Negative log likelihood is {:.5f},\
                        mmd loss is {:.5f} closs {:.5f} batch-accuracy {:.5f}".format(
                    item, len(dl),
                    epoch, num_epoch, l2loss.item(),
                    variational_loss.item(), closs.item(), cmetric))

            if epoch % plot_every == 0 and item == 0:
                # print the distribution of laten space
                distro_mu = z.mean(dim=0)
                gen_z = torch.randn(10 * 10, zdim, requires_grad=False)
                if use_cuda == 1:
                    gen_z = gen_z.cuda()
                    distro_mu = distro_mu.cuda()
                with torch.no_grad():
                    samples = model.decoder(gen_z + distro_mu)
                    samples = samples * (sigmas.mean(dim=0) + 1e-7) + mus.mean(dim=0)
                plotter.imagesc(10, samples, env_name=environment_name + "_F")

                # plot the reconstructed batch
                x_reconstructed = x_reconstructed * (sigmas + 1e-7) + mus
                action_frames_batch_reconstructed = x_reconstructed.view(slist)
                action_frames_batch_norm = (action_image_batch * (sigmas + 1e-7) + mus).view(slist)
                plot_jointvideo_visdom(environment_name + "_R+B",
                                       plotter,
                                       action_frames_batch_norm[0:9, :, :, :, :].cpu(),
                                       action_frames_batch_reconstructed[0:9, :, :, :, :].cpu())
                # plot_video_visdom(environment_name + "_B",
                #                   plotter, action_frames_batch[0:9, :, :, :, :] / 255)
                # plot_video_visdom(environment_name + "_RB",
                #                   plotter, action_frames_batch_reconstructed[0:9, :, :, :, :])

        if epoch % validate_every == 0:
            average_l2loss, average_varloss, average_closs, average_cmetric = validation_step(
                numclasses, testdl, model, transform, discriminator, classification, epoch)
            if best_cmetric <= average_cmetric:
                print(f"found a current best model with {average_cmetric} wrt. {best_cmetric}")
                best_cmetric = average_cmetric
                save_model(epoch,
                           model,
                           transform,
                           discriminator,
                           classification,
                           params.model_filename + f"_{best_cmetric}_{epoch}.pth")

    save_model(epoch, model, transform,
               discriminator, classification, params.model_filename + ".pth")
    with open(params.embedings_filename, 'w') as f:
        with torch.no_grad():
            for item, (action_frames_batch, current_event_type) in enumerate(dl):
                # print(f"seq:{action_frames_batch.shape},label: {current_event_type}")
                action_frames_batch = action_frames_batch.permute(0, 1, 4, 2, 3)
                slist = list(action_frames_batch.shape)
                bdims = [slist[1] * slist[0], slist[2], slist[3], slist[4]]
                action_image_batch = action_frames_batch.view(bdims).to(dtype=torch.float32)
                action_image_batch = (action_image_batch / 255).cuda()
                current_event_type = current_event_type.cuda()

                # TODO: check different z_dims , different true_samples size
                true_samples = torch.randn(batch_size, zdim, requires_grad=False)
                if use_cuda == 1:
                    true_samples = true_samples.cuda()

                # TODO: relacio de Z vs Labels : plot Z amb colors per label
                z, x_reconstructed = model(action_image_batch)
                # action_frames_batch_reconstructed = x_reconstructed.view(slist)
                # plot_video_visdom(environment_name, plotter, action_frames_batch_reconstructed)

                vz = z.view(slist[0], -1, zdim).permute(0, 2, 1).detach()
                clips_embeddings = discriminator(transform(vz)).detach().cpu().numpy()
                clips_embeddings = np.concatenate(
                    (current_event_type.detach().cpu().numpy().reshape(-1, 1), clips_embeddings),
                    axis=1)
                write_to_file(f, clips_embeddings)
