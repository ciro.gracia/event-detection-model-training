import logging
from typing import cast, Optional, Dict, List, Tuple, Union, Any
import numpy as np
import torch
from torch import nn
from torch.utils.data import Dataset
import torchvision


log = logging.getLogger(__name__)

class SimpleClipDataset(torch.utils.data.Dataset):
    """Class to access a set of mini clip video files.

       If a single column list is found (space separated):
        Clip names are expected to have names with:
            id-propertie-propertie-...-eventype.mp4
       If multi column list is found :
        first column is the file name of the clip
        second colum is the assigned label
    Args:
        path_data (str): list of the videos included in the dataset
    """
    def __init__(self,
                 listfile: str,
                 gray_scale: bool = False):

        super().__init__()

        self._gray_scale = gray_scale
        self.list_file: str = listfile
        print(f"reading {self.list_file}")

        gui_lines: List[str] = []
        self.video_filesnames: List[str] = []
        self.labels: List[str] = []

        self.labels_set: np.ndarray
        self.speakerIDs: np.ndarray

        # load the whole file
        with open(self.list_file, 'r') as guifile:
            for line in guifile:
                gui_lines.append(line.strip())

        print(f"found {len(gui_lines)} files")

        # read filenames and extract labels
        for line in gui_lines:
            columns = line.split(' ')
            if len(columns) == 1:
                filename = columns[0]
                self.video_filesnames.append(filename)
                parts = filename.replace('.mp4', '').split('-')
                self.labels.append(parts[-1])
            else:
                self.video_filesnames.append(columns[0])
                self.labels.append(columns[1])

        self.labels_set, self.speakerIDs = np.unique(
            np.array(self.labels),
            return_inverse=True)

        print(self.labels_set, " ", np.amin(self.speakerIDs), "-", np.amax(self.speakerIDs))

    def __len__(self):
        """Retrieve the length of the dataset.

        Returns:
            int : number of clips in the dataset
        """
        return len(self.video_filesnames)

    def numclasses(self):
        """Retrieve the number of classes contained in the dataset.

        Returns:
            int: number of different classes (event types)
        """
        return len(self.labels_set)

    def __getitem__(self, idx
                    ) -> Tuple[torch.Tensor, int]:
        """Read a sample id and its groundtruth from the video/s.

        Args:
            idx (int): index of the dataset sample to retrieve
        Returns:
            Tuple[ Tensor :with clip frames,
                   str: string with the event type
            ]
        """
        current_file = self.video_filesnames[idx]
        current_label_id = int(self.speakerIDs[idx])

        # print(f"reading {current_file} with label {current_label} {current_label_id}")
        vtensor, _, _ = torchvision.io.read_video(current_file, 0, None)
        if vtensor.shape[0] == 0:
            logging.warning(f"{current_file} is broken")
        vtensor = vtensor.to(torch.float)
        if self._gray_scale:
            vtensor = vtensor.mean(dim=3).unsqueeze(3)
        return vtensor, current_label_id
