import torch
import numpy as np
from visdom import Visdom
from typing import Dict, Union
import tempfile
import torchvision


class VisdomLinePlotter(object):
    """Plots to Visdom."""
    def __init__(self, port: int = 6690, env_name: str = 'main'):
        self.vis = Visdom(port=port)
        self.env: str = env_name
        self.plots: Dict[str, int] = {}

    def clear(self, env_name=None):
        """Clear the enviroment."""
        if env_name is None:
            self.vis.delete_env(env_name)
        else:
            self.vis.delete_env(self.env)

    def imagesc(self, nrows, images: torch.Tensor, env_name=None):
        """Print images in rows."""
        if env_name is None:
            self.vis.images(images, nrow=nrows, env=self.env)
        else:
            self.vis.images(images, nrow=nrows, env=env_name)

    def plot(self, var_name, split_name, title_name, x, y):
        """Plot a function of append data to existing line."""
        if var_name not in self.plots:
            self.plots[var_name] = self.vis.line(X=np.array([x, x]),
                                                 Y=np.array([y, y]),
                                                 env=self.env,
                                                 opts=dict(
                                                     legend=[split_name],
                                                     title=title_name,
                                                     xlabel='Epochs',
                                                     ylabel=var_name))
        else:
            self.vis.line(X=np.array([x]), Y=np.array([y]),
                          env=self.env, win=self.plots[var_name],
                          name=split_name, update='append')


def plot_video_visdom(env_name, plotter, clips):
    """Plot a video batch in visdom."""
    tmpf = tempfile.NamedTemporaryFile(suffix=".mp4")
    filename = tmpf.name
    tmpf.close()
    video_opts = dict(fps=10, autoplay=True, loop=True)
    for sequence in range(0, clips.shape[0]):
        clip = (clips[sequence, :, :, :, :] * 255).to(torch.uint8).permute(0, 2, 3, 1).cpu()
        if clip.shape[3] == 1:
            clip = clip.repeat((1,1,1,3))
        torchvision.io.write_video(filename,
                                   clip,
                                   3, 'libx264')
        plotter.vis.video(videofile=filename,
                          env=env_name, opts=video_opts)


def plot_jointvideo_visdom(env_name, plotter, clips, rclips):
    """Plot two batches of videos concatenated in vertical."""
    jointclip = torch.cat([clips, rclips], dim=3)
    plot_video_visdom(env_name, plotter, jointclip)
